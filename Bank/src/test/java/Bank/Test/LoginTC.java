package Bank.Test;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.bank.pages.LoginPage;
import com.bank.pages.NewCustomerDetailsPage;
import com.bank.testBase.FrameworkBase;

public class LoginTC extends FrameworkBase {

	@BeforeTest
	public void browserLaunch() {
		browserInitialization();
	}

	@Test(priority = 1)
	public void verifyTitlePage() {
		String getTitle = driver.getTitle();
		System.out.println("Title 1:" + getTitle);
		Assert.assertEquals(getTitle, "Guru99 Bank Home Page");
	}

	@Test(priority = 2)
	public void loginPage() {
		LoginPage lg = new LoginPage();
		lg.login();

		String getTitle = driver.getTitle();
		System.out.println("Title 2:" + getTitle);
		Assert.assertEquals(getTitle, "Guru99 Bank Manager HomePage");

	}

	@Test(priority = 3)
	public void newCustomer() {
		NewCustomerDetailsPage nc = new NewCustomerDetailsPage();
		nc.NewCustomerBtn.click();
		nc.customerName.sendKeys("Natha Rathod");

	}

}