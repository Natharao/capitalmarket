package com.bank.testBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FrameworkBase {

//************************************************************************************
	public static WebDriver driver;
	public static Properties prop = null;
	public static String path = System.getProperty("user.dir");

	public FrameworkBase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(path + "/src/main/java/com/bank/config/config.properties");
			prop.load(ip);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

//===================================================================================================
	public static void browserInitialization() {

		System.out.println("Browser is not initialized So Initiating new browser instance");

		System.setProperty("webdriver.chrome.driver", path + "/browserDrivers/chromedriver.exe");

//		String browserInstance=prop.getProperty("headless");

		/*
		 * switch ("true") {
		 * 
		 * case "true": System.out.println(
		 * "**********************\n\n\t\t\t\t Currently running on headless browser \n\n\t\t\t\t You can change this from config.properties\n"
		 * ); ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--headless"); driver = new ChromeDriver(options);
		 * 
		 * break;
		 * 
		 * case "remote": System.setProperty("webdriver.chrome.driver",
		 * "/usr/local/bin/chromedriver"); final ChromeOptions chromeOptions = new
		 * ChromeOptions(); chromeOptions.addArguments("--headless"); driver = new
		 * ChromeDriver(chromeOptions); break;
		 * 
		 * case "false": System.out.println(
		 * "**********************\n\n\t\t\t Browser will be launched since headless config is set to false \n\n\t\t\t\t You can change this from config.properties\n"
		 * ); driver = new ChromeDriver(); driver.manage().window().maximize();
		 * 
		 * break;
		 * 
		 * default: System.out.println(
		 * "**********************\n\n\t\t\t Browser will be launched since headless config NOT found \n\n\t\t\t\t You can change this from config.properties\n"
		 * ); driver = new ChromeDriver(); driver.manage().window().maximize();
		 * 
		 * }
		 */
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(prop.getProperty("url"));

	}

	/****************************************************************************/

	public void click(WebElement element) {

		driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
		try {
			if (element.isDisplayed()) {
				WebDriverWait wait = new WebDriverWait(driver, 3);

				wait.until(ExpectedConditions.elementToBeClickable(element));
				element.click();

			} else {
				if (element.isDisplayed()) {

					WebDriverWait wait = new WebDriverWait(driver, 3);

					wait.until(ExpectedConditions.elementToBeClickable(element));

					((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

				}
			}
		} catch (Exception e) {

			try {
				if (element.isDisplayed()) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
				} else {
				}

			} catch (Exception w) {

				Actions actions = new Actions(driver);
				actions.moveToElement(element);
				actions.click();
			}

		}

	}

	public void clickBtn(String buttonText) {
		String xPathBtn = "//button[contains(text(),'" + buttonText + "')]";
		WebElement btnclk = driver.findElement(By.xpath(xPathBtn));
		click(btnclk);

	}

	public String getFromRepo(String keys) {

		String value = null;

		Properties prop4 = new Properties();
		File filepath = new File(path + "/src/main/java/com/bank/testdata/TestData.properties");
		FileInputStream orderType = null;
		try {
			orderType = new FileInputStream(filepath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			prop4.load(orderType);
		} catch (IOException e) {
			e.printStackTrace();
		}

		value = prop4.getProperty(keys);

		return value;
	}

}
