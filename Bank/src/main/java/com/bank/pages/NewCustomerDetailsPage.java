package com.bank.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bank.testBase.FrameworkBase;

public class NewCustomerDetailsPage extends FrameworkBase {

	@FindBy(xpath = "//input[@name='name']")
	public WebElement customerName;

	@FindBy(xpath = "//input[@name='password']")
	WebElement passWord;

	@FindBy(xpath = "//a[text()='New Customer']")
	public WebElement NewCustomerBtn;
	

	public NewCustomerDetailsPage() {
		
		PageFactory.initElements(driver, this);
	}
	
	
}
