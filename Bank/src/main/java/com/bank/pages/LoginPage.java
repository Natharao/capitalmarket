package com.bank.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bank.testBase.FrameworkBase;

public class LoginPage extends FrameworkBase {

	@FindBy(xpath = "//input[@name='uid']")
	WebElement userName;

	@FindBy(xpath = "//input[@name='password']")
	WebElement passWord;

	@FindBy(xpath = "//input[@name='btnLogin']")
	WebElement loginBtn;

	public LoginPage() {

		PageFactory.initElements(driver, this);
	}

	public void login() {
		PageFactory.initElements(driver, this);
		userName.sendKeys(prop.getProperty("username"));
		passWord.sendKeys(prop.getProperty("password"));
		loginBtn.click();
	}

}
